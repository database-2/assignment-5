package database320;
import java.sql.Date;

public class Book {
    private String isbn;
    private String title;
    private Date pubdate = new Date(1);
    private int pubid;
    private double cost;
    private double retail;
    private double discount;
    private String category;
    private Publisher publisher;

    public Book(String isbn, String title, Date pubdate, int pubid, double cost, double retail, double discount, String category){
        this.isbn = isbn;
        this.title = title;
        this.pubdate = pubdate;
        this.pubid = pubid;
        this.cost = cost;
        this.retail = retail;
        this.discount = discount;
        this.category = category;
    }

    public String toString(){
        return this.isbn + ": " + this.title + " is published in" + this.pubdate + " and it costs " + this.cost + "$. " + "Its retail is " + this.retail + " and it's discounted to " + this.discount + ". The book's category is " + this.category + " and the publisher is " + this.publisher + ". :D";
    }

    public String getISBN(){
        return this.isbn;
    }

    public String getTitle(){
        return this.title;
    }
    
    public Date getPubdate(){
        return this.pubdate;
    }
    
    public int getPubid(){
        return this.pubid;
    }
    
    public double getCost(){
        return this.cost;
    }
    
    public double getRetail(){
        return this.retail;
    }
    
    public double getDiscount(){
        return this.discount;
    }
    
    public String getCategory(){
        return this.category;
    }
}
