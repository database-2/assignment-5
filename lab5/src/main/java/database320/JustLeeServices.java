package database320;
import java.sql.*;
import java.util.Scanner;

public class JustLeeServices {
    public static Connection conn;
    public static void main(String[] args) throws SQLException{
        Date date = Date.valueOf("2023-09-29"); 
        Connection conn = null;
        Scanner reader = new Scanner(System.in);
        System.out.println("Enter a username:");
        String username = reader.next();
        System.out.println("Enter a password:");
        String password = reader.next();

        reader.close();

        try{
            conn = getConnection(username, password);
            Book b = new Book("1059851194", "Rida doesn't like cucumbers", date, 2, 120.00, 90.00, 20, "SELF-HELP");
            addBook(b);

        }catch(SQLException sql){
            System.out.println(sql.getMessage());
            conn.rollback();    
        }finally{
            conn.close();
        }

    }


    public static Connection getConnection(String username, String password) throws SQLException{
        Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca", username, password);
        System.out.println("Connected to database");

        return conn;
        
    }
    public static Book getBook(String isbn) throws SQLException{
        
        String bookInfo = "SELECT * FROM Books WHERE isbn = ?";
        Book book = null;

        PreparedStatement stmnt = conn.prepareStatement(bookInfo);
        ResultSet results = stmnt.executeQuery();

        stmnt.setString(1, isbn);

        if(results != null) {
            String title = results.getString("title");
            Date pubdate = results.getDate("pubdate");
            int pubid = results.getInt("pubid");
            double cost = results.getDouble("cost");
            double retail = results.getDouble("retail");
            double discount = results.getDouble("discount");
            String category = results.getString("category");

            book = new Book(isbn, title, pubdate, pubid, cost, retail, discount, category);
        }

        return book;
    }
/* 
    public static Publisher getPublisher(int pubid) throws SQLException{
        String pubInfo = "SELECT name FROM Publisher WHERE pubid = ?";
        Publisher pub = null;
        Connection conn = getConnection();
        PreparedStatement stmnt = conn.prepareStatement(pubInfo);
        ResultSet results = stmnt.executeQuery();

        stmnt.setInt(1, pubid);
             
        if(results != null){
            String name = results.getString("name");
            String contact = results.getString("contact");
            String phone = results.getString("phone");


            pub = new Publisher(pubid, name, contact, phone);
        }
        return pub;
    }
*/
    public static void addBook(Book b) throws SQLException{
        String add = "INSERT INTO Books(isbn, title, pubdate, pubid, cost, retail, discount, category) VALUES(?, ?, ?, ?, ?, ?, ?, ?);";
        PreparedStatement stmnt = conn.prepareStatement(add);
        

        stmnt.setString(1, b.getISBN());
        stmnt.setString(2, b.getTitle());
        stmnt.setDate(3, b.getPubdate());
        stmnt.setInt(4, b.getPubid());
        stmnt.setDouble(5, b.getCost());
        stmnt.setDouble(6, b.getRetail());
        stmnt.setDouble(7, b.getDiscount());
        stmnt.setString(8, b.getCategory());
    
        stmnt.executeUpdate();
    
        conn.commit();
        
    }
     
}
