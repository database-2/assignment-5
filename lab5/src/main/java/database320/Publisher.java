package database320;


public class Publisher {
    private int pubid;
    private String name;
    private String contact;
    private String phone;

    public Publisher(int pubid, String name, String contact, String phone){
        this.pubid = pubid;
        this.name = name;
        this.contact = contact;
        this.phone = phone;
    }
}
